import os
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

for i in range(21):
    #
    # Get filename
    # os.path.dirname(__file__) gives the path of this source file
    #
    filename = "myresult{:03d}.dat".format(i)
    filepath = os.path.join(os.path.dirname(__file__),"..", "fortranProgramThatWritesData", "results", filename)

    #
    # Read data with numpy:
    #
    data = np.loadtxt(filepath)
    
    #
    # plot data
    #
    ax = plt.gca()
    ax.clear()
    plt.plot(data[:,0], data[:,1]) 
    plt.pause(0.5)

plt.show()

