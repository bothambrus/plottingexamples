program my_program
    implicit none
    integer             :: npoin, nstep
    integer             :: ipoin, istep
    real                :: xmax, xmin
    real, pointer       :: x(:)
    real, pointer       :: u(:)

    !
    ! Parameters
    !
    npoin = 101
    nstep = 20
    xmin = 0
    xmax = 0.314159

    !
    ! Allocation
    !
    allocate(x(npoin))
    allocate(u(npoin))

    !
    ! Initialize variables
    !
    do ipoin = 1,npoin
        x(ipoin) = xmin + (xmax-xmin) * real(ipoin-1)/(npoin-1)
    enddo
 
    !
    ! Do some steps
    !
    do istep = 0,nstep
        call my_function(istep,npoin,x,u)
    enddo

    !
    ! Deallocate
    !
    deallocate(x)
    deallocate(u)

end program my_program
