subroutine my_function(istep,npoin,x,u)
    implicit none
    integer,       intent(in)    :: istep
    integer,       intent(in)    :: npoin
    real,          intent(in)    :: x(npoin)
    real,          intent(inout) :: u(npoin)
    
    !
    ! Local variables
    !
    integer        :: ipoin
    real           :: t, lambda, f, omega, k
    character(40)  :: fn 


    !
    ! Give value based on step
    !
    t = 0.01*real(istep)

    lambda = 0.314159 * 0.5
    f      = 10.0

    k      = 2.0*3.14159 / lambda
    omega  = 2.0*3.14159 * f 
    u      = sin(k*x - omega*t)

    !
    ! File name
    !
    write(fn,("('results/myresult',i0.3,'.dat')")) istep 
    print*, "Write step:", istep, ", t=", t, " to file: ", trim(fn)

    call system("mkdir -p results")
    open( 666, file=trim(fn) )
    do ipoin = 1,npoin
      write(666,"(f12.8,x,f12.8)") x(ipoin), u(ipoin)
    enddo
    close(666)

     


end subroutine my_function
